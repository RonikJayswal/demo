//
//  CalculationViewController.swift
//  Partic
//
//  Created by Ronik on 03/07/20.
//  Copyright © 2020 Ronik. All rights reserved.
//

import UIKit

class CalculationViewController: UIViewController {
    
    
    @IBOutlet weak var showLabelVar: UILabel!
    @IBOutlet weak var answerLabelVar: UILabel!
    @IBOutlet var zeroto9BtVar: [UIButton]!
    
    @IBOutlet var opraterVar: [UIButton]!
    
    var showValue : String = ""
    var mainValue : Double  = 0
    var answerValue : Double  = 0
     var value1 : Double  = 0
     var value2 : Double  = 0
    var isCheckValue = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        showLabelVar.text = ""
        answerLabelVar.text = "0"
        answerLabelVar.isHidden = true
    }
    
    
    @IBAction func zzeroto9btAction(_ sender: UIButton) {
        
        print(sender.tag)
        zeroto9ButtonClick(tag: sender.tag)
    }
    
    @IBAction func opraterActionBt(_ sender: UIButton) {
        print(sender.tag)
        operaterActionClick(tag: sender.tag)
        
    }
    
    
    
    
    
}


//  numbers[digit] Click time function inside
extension CalculationViewController
{
    func zeroto9ButtonClick(tag : Int )
    {
        answerLabelVar.isHidden = false
        
        switch tag {
            
        case 0:
            showValue += "0"
            mainValue = (mainValue * 10) + 0
            showLabelVar.text = String(showValue)
            
        case 1:
            if isCheckValue == 0
            {
                showValue += "1"
                mainValue = (mainValue * 10) + 1
                showLabelVar.text = String(showValue)
            }else
            {
                showValue += "1"
                value1 = mainValue
                value2 = (value2 * 10) + 1
                showLabelVar.text = String(showValue)
                checkoprater(value1: value1, value2: value2)
                
            }
            
            
          
            
            
        case 2:
            if isCheckValue == 0
                      {
            showValue += "2"
            mainValue = (mainValue * 10) + 2
            showLabelVar.text = String(showValue)
            //isChecoprater(value: Int(mainValue))
            }
            else
            {
                showValue += "2"
                value1 = Double(answerLabelVar.text!)!
                value2 =  2
                showLabelVar.text = String(showValue)
                checkoprater(value1: value1, value2: value2)
                
            }
            
        case 3:
            if isCheckValue == 0
            {
            showValue += "3"
            mainValue = (mainValue * 10) + 3
            showLabelVar.text = String(showValue)
           // isChecoprater(value: Int(mainValue))
            }else
            {
                showValue += "3"
                value1 = mainValue
                value2 =  3
                showLabelVar.text = String(showValue)
                checkoprater(value1: value1, value2: value2)
                
            }
        case 4:
            if isCheckValue == 0
            {
            showValue += "4"
            mainValue = (mainValue * 10) + 4
            showLabelVar.text = String(showValue)
            //isChecoprater(value: Int(mainValue))
            }else
            {
                showValue += "4"
                value1 = mainValue
                value2 = (value2 * 10) + 2
                showLabelVar.text = String(showValue)
                checkoprater(value1: value1, value2: value2)
                
            }
            
        case 5:
            if isCheckValue == 0
            {
            showValue += "5"
            mainValue = (mainValue * 10) + 5
            showLabelVar.text = String(showValue)
            //isChecoprater(value: Int(mainValue))
            }
            else
            {
                showValue += "5"
                value1 = mainValue
                value2 = (value2 * 10) + 2
                showLabelVar.text = String(showValue)
                checkoprater(value1: value1, value2: value2)
                
            }
            
        case 6:
        if isCheckValue == 0
        {
            showValue += "6"
            mainValue = (mainValue * 10) + 6
            showLabelVar.text = String(showValue)
            //isChecoprater(value: Int(mainValue))
            }
            else
            {
                showValue += "6"
                value1 = mainValue
                value2 = (value2 * 10) + 2
                showLabelVar.text = String(showValue)
                checkoprater(value1: value1, value2: value2)
                
            }
            
        case 7:
            if isCheckValue == 0
            {
            showValue += "7"
            mainValue = (mainValue * 10) + 7
            showLabelVar.text = String(showValue)
          // isChecoprater(value: Int(mainValue))
            }
            else
            {
                showValue += "7"
                value1 = mainValue
                value2 = (value2 * 10) + 2
                showLabelVar.text = String(showValue)
                checkoprater(value1: value1, value2: value2)
                
            }
        case 8:
            if isCheckValue == 0
            {
            showValue += "8"
            mainValue = (mainValue * 10) + 8
            showLabelVar.text = String(showValue)
            //isChecoprater(value: Int(mainValue))
            }
            else
            {
                showValue += "8"
                value1 = mainValue
                value2 = (value2 * 10) + 2
                showLabelVar.text = String(showValue)
                checkoprater(value1: value1, value2: value2)
                
            }
            
        case 9:
            if isCheckValue == 0
            {
            showValue += "9"
            mainValue = (mainValue * 10) + 9
            showLabelVar.text = String(showValue)
            //isChecoprater(value: Int(mainValue))
            }
            else
            {
                showValue += "9"
                value1 = mainValue
                value2 = (value2 * 10) + 2
                showLabelVar.text = String(showValue)
                checkoprater(value1: value1, value2: value2)
                
            }
        case 10:
            showValue += "00"
            mainValue = (mainValue * 100) + 0
            showLabelVar.text = String(showValue)
            //isChecoprater(value: Int(mainValue))
        case 11:
            var lastCharater = showValue.last ?? " "
            if lastCharater  != "."
            {
                showValue += "."
                //                     mainValue = (mainValue * 100) + 0
                showLabelVar.text = String(showValue)
            }
            
        default:
            print("chek you click")
        }
    }
    
}


//extension for opreater click event

extension CalculationViewController
{
    func  operaterActionClick(tag : Int)
    {
        let lastCharater = showValue.last
        switch tag {
        case 101:
            showLabelVar.text = ""
            answerLabelVar.text = ""
            showValue = ""
            answerValue = 0
            mainValue = 0
        case 102:
            if lastCharater != "/" && lastCharater != "*" && lastCharater != "-" && lastCharater != "+"
            {
                showValue += "%"
                isCheckValue = 102
                showLabelVar.text = String(showValue)
            }
        case 103:
            
            showValue = String(showValue.dropLast())
            showLabelVar.text = showValue
            
        case 104:
            if lastCharater != "/" && lastCharater != "*" && lastCharater != "-" && lastCharater != "+"
            {
                print(lastCharater)
                showValue += "/"
                isCheckValue = 104
                showLabelVar.text = String(showValue)
            }
            
        case 105:
            if lastCharater != "/" && lastCharater != "*" && lastCharater != "-" && lastCharater != "+"
            {
                showValue += "*"
                isCheckValue = 105
                showLabelVar.text = String(showValue)
            }
        case 106:
            if lastCharater != "/" && lastCharater != "*" && lastCharater != "-" && lastCharater != "+"
            {
                showValue += "-"
                isCheckValue = 106
                showLabelVar.text = String(showValue)
            }
        case 107:
            if lastCharater != "/" && lastCharater != "*" && lastCharater != "-" && lastCharater != "+"
            {
                showValue += "+"
                isCheckValue = 107
                showLabelVar.text = String(showValue)
            }
        default:
            print("other click")
        }
    }
    
    func  checkoprater(value1 : Double ,value2 : Double )
    {
        var answer : Double
        if isCheckValue == 104
        {
           answer = value1 / value2
            answerLabelVar.text = String(answer)
        }else if isCheckValue == 105
        {
           answer = value1 * value2
           answerLabelVar.text = String(answer)
        }
        else if isCheckValue == 106
        {
           answer = value1 - value2
           answerLabelVar.text = String(answer)
        }
        else if isCheckValue == 107
        {
           answer = value1 + value2
           answerLabelVar.text = String(answer)
        }
        
      
   
}

    
}

