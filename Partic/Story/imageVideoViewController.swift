//
//  imageVideoViewController.swift
//  Partic
//
//  Created by Ronik on 08/07/20.
//  Copyright © 2020 Ronik. All rights reserved.
//

import UIKit

class imageVideoViewController: UIViewController {

    @IBOutlet weak var subViewVar: UIView!
    
    @IBOutlet weak var imageViewVAr: UIImageView!
    
    @IBOutlet weak var viewcontrolerSetViewVAr: UIView!
    
    var imageArray  : [UIImage] = [#imageLiteral(resourceName: "team6"),#imageLiteral(resourceName: "team3"),#imageLiteral(resourceName: "team1"),#imageLiteral(resourceName: "team2"),#imageLiteral(resourceName: "team4"),#imageLiteral(resourceName: "team5")]
    var timers = Timer()
    var counter = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        imageViewVAr.image = imageArray[counter]
        timers = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(changeImage), userInfo: nil, repeats: true)
        
        var leftSwipes = UISwipeGestureRecognizer(target: self , action: #selector(handleSwipes(_:)))
        var rightSwipes = UISwipeGestureRecognizer(target: self , action: #selector(handleSwipes(_:)))
        
        leftSwipes.direction = .left
        rightSwipes.direction = .right
        subViewVar.addGestureRecognizer(leftSwipes)
        subViewVar.addGestureRecognizer(rightSwipes)
    }
    
    // timer change Image
    @objc func changeImage()
    {
        
        if counter < imageArray.count
        {
            
            imageViewVAr.image = imageArray[counter]
            counter += 1
            print("counter",counter, "\n imageCoun = \(imageArray.count)")
        }
      
    }
    // image swipe handler
    @objc func handleSwipes(_ sender : UISwipeGestureRecognizer)
    {
        if sender.direction == .left && counter < imageArray.count - 1
        {
            print("left swipe")
            counter += 1
            imageViewVAr.image = imageArray[counter]
        }
        
        if sender.direction == .right && counter != 0
        {
             print("Right swipe")
            counter -= 1
            imageViewVAr.image = imageArray[counter]
        }
        
        
        
    }
}
