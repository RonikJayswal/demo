//
//  ViewController.swift
//  Partic
//
//  Created by Ronik on 30/06/20.
//  Copyright © 2020 Ronik. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var clickBtVar: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewdidload")
        // Do any additional setup after loading the view.
    }

    @IBAction func clickBt(_ sender: Any) {
        let onboarding = UINavigationController(rootViewController: OnboardingController())
               onboarding.navigationBar.prefersLargeTitles = false// true
               onboarding.navigationBar.tintColor = .white
               onboarding.navigationBar.shadowImage = UIImage()
               
//        let largeTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "SFProDisplay-Bold", size: 34)!]
//               onboarding.navigationBar.largeTitleTextAttributes = largeTextAttributes
               
//        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: "SFProDisplay-Semibold", size: 17)!]
//               onboarding.navigationBar.titleTextAttributes = textAttributes
               onboarding.modalPresentationStyle = .fullScreen
               onboarding.navigationController?.setNavigationBarHidden(true, animated: false)
               present(onboarding, animated: true, completion: nil)
    }
    
}

